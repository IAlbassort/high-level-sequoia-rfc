# 1.0.0
- Initial Release. See 1.0.0 README.md
# 1.0.1
- Updated docs, fixed missing instruction for updating
# 1.0.2
- Updated bitfield proc in accordance with ElegantBeefs's elegant advice
# 1.0.3
- Updated install scripts