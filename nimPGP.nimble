version       = "3"
author        = "Caroline Marceano [Albassort]"
description   = "A high-level and easy to use PGP library. Using Rust & Sequoia-PGP on the backend!"
license       = "MIT"
srcDir        = "src/"
requires "nim >= 2.0.6"

task test, "General tests":
    exec "nim r --hints:off " & "src/tests/ut.nim"


before install:
    if not defined(Linux):
        quit("Only linux is supported currently...")
after install:
    echo "  ---Begin Instructions---\n"
    echo "  Although the nim-side of the library is installed, the rust side is not!"
    echo "  Please go into your ~/.nimble/pkgs2/nimPGP* and install libnimPGP"

    echo "  To install from source:" 
    echo "  ./scripts/link.sh" 
    echo "  Note: This will install to /usr/lib/libnimpgp.so and /usr/include/libnimpgp.h respectively.\n"
    echo "  If you wish to install from a precompiled binary (only available for linux-x86)"
    echo "  https://publiccdn.albassort.com/nimPGP/releases/"
    echo "  ---End Instructions---"
