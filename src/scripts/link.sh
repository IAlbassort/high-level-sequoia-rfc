#!/bin/bash
./scripts/sequoiaprepare.sh
sudo rm -f /usr/include/nimPGP.h
sudo rm -f /usr/lib/libnimPGP.so
sudo ln -v -s $(pwd)/headers/nimPGP.h /usr/include/nimPGP.h
sudo ln -v -s $(pwd)/rust/target/release/libnimPGP.so /usr/lib/libnimPGP.so
#to stave off privlidge escalation
sudo chmod 444 /usr/lib/libnimPGP.so
sudo chmod 444 /usr/include/nimPGP.h

echo NOTE: Sequoia depends on the following: clang cargo llvm pkg-config nettle>
echo If you are on Debian/a Debain Dervied OS:
echo You can run ./scripts/install-deps-apt.sh
echo Otherwise, you must find them yourself
