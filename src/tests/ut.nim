#This doubles as a memory-leak test.
import ../nimPGP
import std/unittest
import std/sequtils
import std/sugar
import std/random
import std/math
import std/strutils
import std/tables
import std/json
import std/jsonutils

type jsonArchive = object
    comment : string
    private : string
    has_password : bool
    should_parse : bool
    password : string
var gpgCerts : seq[Cert]
var reallyBigRandom : string
randomize()

suite "Tests that should work":
    test "CreateGpgKey":
        let encryptionKeys = {ForTransport,  ForStorageEncryption}
        let singingKeys = {ForCertOfOtherKeys,  ForSigning, ForAuthentication}

        let suites = collect(for x in 0 .. 6: CryptSuites(x))
        echo suites
        for suite in suites:
            let subkeyEncryption = newSubkey(encryptionKeys, suite)
            let subkeySigning = newSubkey(singingKeys, suite)
            echo suite
            gpgCerts.add newCert(@[subkeyEncryption, subkeySigning, subkeyEncryption], suite, user_ids = @["test! <test@dev.null>"]) 
        
        var emptySet : set[KeyTypes]
        let subkeySigning = newSubkey(emptySet)
        var isException = false
        try:
            discard newCert(@[subkeySigning])
        except CertGenerationFailedException, AssertionDefect:
            isException = true
        check isException

    test "Signing and Verification":
        # the input cannot have null in the string
        # oddly, not because it become a cstring, but rather, because sequoia omits them, for good reason,
        reallyBigRandom = collect(for x in 0..2^16: char(rand(1 .. 255))).join("")
        for cert in gpgCerts:
            var signedMessage = signMessage(cert.private, cert.public)  
            var verification = verifySignature(cert.public, signedMessage)
            check verification.body == cert.public
            check verification.isValid

            let smallString = "hello"
            signedMessage = signMessage(cert.private, smallString)  
            verification = verifySignature(cert.public, signedMessage)
            check verification.body == smallString
            check verification.isValid

            let emptyString = ""
            signedMessage = signMessage(cert.private, emptyString)  
            verification = verifySignature(cert.public, signedMessage)
            check verification.body == emptyString
            check verification.isValid

        
            signedMessage = signMessage(cert.private, reallyBigRandom)  
            verification = verifySignature(cert.public, signedMessage)
            check verification.body == reallyBigRandom
            check verification.isValid

        var isException = false
        try:
            discard signMessage(gpgCerts[0].private, gpgCerts[0].public, "non-existent key")  
        except CatchableError:
            isException = true
        check isException

        #Doing it this way is pointless because, without using subkey / keyid it will do basically the same thing
        #You would do this if you configure your program to use different keys for different things.
        let signingKey = gpgCerts[0].subkeys.values.toSeq().filter(x => ForSigning in x.keyFlags)[0]

        #Checks if you can get by signing_id
        discard signMessage(gpgCerts[0].private, gpgCerts[0].public, signingKey)  

    test "Encrypting and Decrypting":
        for cert in gpgCerts:
            var signedMessage = sendMessage(cert.public, $cert.public)  
            var decryption = decryptMessage(cert.private, signedMessage)
            check decryption.successful
            check decryption.body == $cert.public

            let smallString = "hello"
            signedMessage = sendMessage(cert.public, smallString)  
            decryption = decryptMessage(cert.private, signedMessage)
            check decryption.successful
            check decryption.body == smallString

            let emptyString = ""
            signedMessage = sendMessage(cert.public, emptyString)  
            decryption = decryptMessage(cert.private, signedMessage)
            check decryption.successful
            check decryption.body == emptyString


            signedMessage = sendMessage(cert.public, $cert.public)  
            decryption = decryptMessage(cert.private, signedMessage)
            check decryption.successful
            check decryption.body == $cert.public

    test "Revocation and Verification of Revocation":
        for cert in gpgCerts:
            check isPrimaryKeyRevoked(cert.public) == NotAsFarAsWeKnow
            let revoked = revokePrimaryKey(cert)
            check isPrimaryKeyRevoked(revoked.public) == Revoked

suite "Tests that shouldn't work":
    test "Incorrect generation":
        var isError = false 
        let bothSingingAndTransport = {ForSigning,  ForTransport}
        let key = newSubkey(bothSingingAndTransport)   
        try:
            discard newCert(@[key])
        except CertGenerationFailedException:
            isError = true
        check isError

        isError = false
        let goodFlag = {ForSigning}
        let badKey = newSubkey(goodFlag, Cv25519, uint32 55)
        let goodKey = newSubkey(goodFlag, Cv25519, uint32 101)

        discard newCert(@[goodKey])

        isError = false
        try:
            discard newCert(@[goodKey], validLength = uint32 55)
        except MustBeLiveForMoreThan100SecondsException:
            isError = true
        check isError

        isError = false
        try:
            discard newCert(@[badKey])
        except MustBeLiveForMoreThan100SecondsException:
            isError = true
        check isError
        
    test "Wrong Public Key For Signing Verification":
        var signedMessage = signMessage(gpgCerts[0].private, gpgCerts[0].public)  
        var verification = verifySignature(gpgCerts[1].public, signedMessage)
        check verification.body == gpgCerts[0].public
        check verification.isValid == false

    test "Incorrect Private Key For Decryption":
        var encrypedMessage = sendMessage(gpgCerts[0].public, gpgCerts[0].public)  
        let decryption = decryptMessage(gpgCerts[1].private, encrypedMessage)

        check decryption.successful == false
        check decryption.body == ""
    
    test "Public when private is wanted":
        var isError = true
        try:
            discard signMessage(gpgCerts[0].public, gpgCerts[0].public)  
        except ExpectedPrivateKeyGotPublicException:
            isError = true
        check isError
        isError = false
        try:
            #It should detect that its public before it parses the message.
            discard decryptMessage(gpgCerts[0].public, "")  
        except ExpectedPrivateKeyGotPublicException:
            isError = true
        check isError
        isError = false
    test "Signing with wrong key":
        let transportKey = newSubkey({ForTransport})
        let cert = newCert(@[transportKey])
        let subkeyid = cert.subkeys.keys.toSeq()[0]
        var isError = false
        try:
            discard cert.private.signMessage("whaever", keyid = subkeyid)
        except IncorrectKeyFlagsException:
            isError = true
        check isError
suite "Tests for groups of keys":
    test "Signing Verification":
        let signed = signMessage(gpgCerts[0].private, gpgCerts[0].public)
        var verification = verifySignature(gpgCerts.map(x => x.public), signed)
        check verification.isValid == true
        verification = verifySignature(gpgCerts[1 .. ^1].map(x => x.public), signed)
        check verification.isValid == false

    test "Encrypting And Decrypting":
        #Sends to all keys in the cert
        let message = sendMessage(gpgCerts.map(x => x.public), gpgCerts[0].public)
        #decrypt with a group
        let decryption = decryptMessage(gpgCerts.map(x => x.private), message)

        check decryption.successful
        check decryption.body == gpgCerts[0].public
        for cert in gpgCerts:
            let decryption = decryptMessage(cert.private, message)
            check decryption.successful
            check decryption.body == gpgCerts[0].public

proc testCertDecryption(a : Cert, b: AtomicPasswordHandler) : bool =
        var isException = false
        try:
            discard signMessage(a.private, "hello")   
        except IncorrectPasswordForSecretKeyException:
            isException = true
        check isException
        isException = false
        try:
            discard signMessage(a.private, "hello", passwordHandler = newPasswordHandler("wrong_password") )   
        except IncorrectPasswordForSecretKeyException:
            isException = true
        check isException
        let signedMessage = signMessage(a.private, "hello", passwordHandler = b)
        let signature = verifySignature(a.public, signedMessage)

        check signature.isValid == true
        check signature.body == "hello"

        let encryptedMessage = sendMessage(a.public, "hello")
        let decryptMessage = decryptMessage(a.private, encryptedMessage, passwordHandler = b)
        check decryptMessage.body == "hello"

        isException = false
        try:
            discard decryptMessage(a.private, encryptedMessage)
        except CatchableError:
            isException = true
        check isException
        isException = false

        try:
            discard decryptMessage(a.private, encryptedMessage, passwordHandler = convertGlobalPasswordToAtomic(a, newPasswordHandler("wrong_password")))
        except CatchableError:
            isException = true
        check isException
        

        let revokedPrimary = revokePrimaryKey(a, passwordHandler = b)
        check isPrimaryKeyRevoked(revokedPrimary.public) == Revoked
        isException = false
        try:
            let revoked = revokePrimaryKey(a)
            discard isPrimaryKeyRevoked(revoked.public)

        except CatchableError:
            isException = true
        check isException
        isException = false
        try:

            let revoked = revokePrimaryKey(a, passwordHandler = newPasswordHandler("Wrong Password"))
            discard isPrimaryKeyRevoked(revoked.public)

        except CatchableError:
            isException = true
        check isException

        return true

suite "Password Tests":
    test "Testing global key encryption":
        let simplePassword = "good password"
        let handler = newPasswordHandler simplePassword 
        for i in gpgCerts:
            let encrypted = encryptKeys(gpgCerts[0], handler)
            var isException = false
            try:
                discard encryptKeys(encrypted, handler)   
            except FoundEncryptedSecretButNoPasswordHandlerException:
                isException = true
            check isException
            check testCertDecryption(encrypted, convertGlobalPasswordToAtomic(encrypted, handler))
    test "Atomic Encryption Test":
        let testingCert = gpgCerts[0]
        let keyids = testingCert.subkeys.keys.toSeq()
        let passwordOne = newPasswordHandler {testingCert.primaryKeyId : "primary-key_password", keyids[0] : "test", keyids[1] : "test2"}.toTable()
        let passwordTwo = newPasswordHandler {testingCert.primaryKeyId : "primary-key_password", keyids[0] : "newPassword", keyids[1] : "newPassword2"}.toTable()
        
        let newCert = encryptKeys(testingCert, passwordOne)
        let reEncryptedCert = encryptKeys(newCert, passwordTwo, passwordOne)
        check testCertDecryption(reEncryptedCert, passwordTwo)
    test "Creating password keyring":
        let pwArray = [newPasswordHandler("two"), newPasswordHandler("one"), newPasswordHandler("three")]
        let subkey = newSubkey({ForTransport})
        let certs = collect(for x in 0 .. 2: encryptKeys(newCert(@[subkey]), pwArray[x]))
        let atomicPws = collect(for x in 0 .. 2: convertGlobalPasswordToAtomic(certs[x], pwArray[x]))        
        let messages = [certs[0].public.sendMessage("0"), certs[1].public.sendMessage("1"), certs[2].public.sendMessage("2")]
        let macroHandler = newPasswordHandlerKeyring(atomicPws)
        for x in 0 .. messages.high:
            let decrypted = decryptMessage(certs.map(x=>x.private), messages[x], macroHandler)
            check decrypted.successful == true
suite "Test Scaffolding":
    test "Externally Generated Keys":
        #Tests certs which should and shouldn't be parsable 
        let json = parseJson(readFile("./foregin_certs.json"))
        for i in json:
            var archive : jsonArchive
            fromJson(archive, i)
            if archive.should_parse:
                discard scaffoldKey(archive.private)
            else:
                var isError = false
                try:
                    discard scaffoldKey(archive.private)
                except CertParsingException:
                    isError = true
                check isError
suite "Dev release prevention":
    test "Check not dev release":
        check nimPGP.version != "DEV"
