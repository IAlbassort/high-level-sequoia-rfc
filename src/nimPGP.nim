import rustBindings/binding
import std/sets
import std/sugar
import std/tables
import std/strutils
import std/sequtils
import std/enumerate
import std/times
import std/strformat
import std/terminal
import std/httpclient
#These types need to be declared in the bindings for rust, and so we just export them here.
export KeyTypes
export ReasonForRevocation
export CryptSuites
export RevocationStatus

const version* = "1.0.3" 
    
type 
    PublicKey* = string
    PrivateKey* = string
    Cert* = object
        public* : PublicKey
        private* : PrivateKey
        primaryKeyId* : string
        subkeys* : Table[string, Subkey]
        creationTime : uint64
        validLengthSeconds : uint64
        expires : bool
        user_ids : seq[string]
        fingerprint : string
    UninitiatedSubkey* = object
        cipher* : CryptSuites = Cv25519
        keyFlags* : set[KeyTypes]
        keyId* : string
        expires* : bool
        validLengthSeconds* : uint64
    Subkey* = object
        keyFlags* : set[KeyTypes]
        keyId* : string
        expires* : bool
        validLengthSeconds* : uint64
        creationTime* : uint64
    Verification* = object
        isValid* : bool
        body* : string
    Decryption* = object
        successful* : bool
        body* : string
    PasswordHandler* = object of RootObj
        useAtomicPassword : bool
        initialized : bool
        keyCount : uint8
        globalPassword : string
        keyids : seq[string]
        passwordByKeyid : seq[string]
    GlobalPasswordHandler* = object of PasswordHandler
    AtomicPasswordHandler* = object of PasswordHandler


    SequoiaException* = object of CatchableError
    CertParsingException* = object of SequoiaException
    MissMatchingKey* = object of SequoiaException
    CertMaybeRevokedException* = object of SequoiaException
    CertRevokedException* = object of SequoiaException
    FailedToWriteMessageException* = object of SequoiaException
    CertGenerationFailedException* = object of SequoiaException
    NoKeyMeetsSelectionException* = object of SequoiaException
    FailedToParseMessageException* = object of SequoiaException
    PackedParserFailedToRecurseException* = object of SequoiaException
    FailedToReadFromBufferException* = object of SequoiaException
    FailedToRevokeSubkeyException* = object of SequoiaException
    FailedToRevokePrimaryKeyException* = object of SequoiaException
    FailedToEncryptKeyException* = object of SequoiaException
    CertHasEncryptedKeyException* = object of SequoiaException
    PasswordException* = object of SequoiaException

    NimSideSequoiaException* = object of SequoiaException

    FailedToParseKeyPublicException* = object of CertParsingException
    FailedToParseKeyPrivateException* = object of CertParsingException
    FailedToParseKeyException* = object of CertParsingException
    CertIsInvalidException = object of CertParsingException

    ExpectedPrivateKeyGotPublicException* = object of MissMatchingKey
    IncorrectKeyFlagsException* = object of MissMatchingKey

    PasswordSetToAtomicButMissingEncryptedKeyidException* = object of PasswordException
    IncorrectPasswordForSecretKeyException* = object of PasswordException
    FoundEncryptedSecretButNoPasswordHandlerException* = object of PasswordException
    #Nim-side 
    CertLacksPrivateKey* = object of NimSideSequoiaException
    MustBeLiveForMoreThan100SecondsException* = object of NimSideSequoiaException
    KeyCannotBeUsedForTransportAndSigningException* = object of CertGenerationFailedException

proc handleExceptions(errorCode : OutwardCommExceptions, raiseMaybeRevoked = false) =
    #Previously I thought using an Assert made sense but this allows for atomic error handling.
    #It also allows me to group exceptions with inheritance for easier handling.
    case errorCode:
        of NoException:
            return
        of FailedToWriteMessage:
            raise new FailedToWriteMessageException
        of ExpectedPrivateKeyGotPublic:
            raise new ExpectedPrivateKeyGotPublicException
        of FailedToParseKeyPublic:
            raise new FailedToParseKeyPublicException
        of FailedToParseKeyPrivate:
            raise new FailedToParseKeyPrivateException
        of NoKeyMeetsSelection:
            raise new NoKeyMeetsSelectionException
        of FailedToParseMessage:
            raise new FailedToParseMessageException
        of PackedParserFailedToRecurse:
            raise new PackedParserFailedToRecurseException
        of FailedToReadFromBuffer:
            raise new FailedToReadFromBufferException
        of CertGenerationFailed:
            raise new CertGenerationFailedException
        of FailedToParseKey:
            raise new FailedToParseKeyException
        of CertRevoked:
            raise new CertRevokedException
        of FailedToRevokeSubkey:
            raise new FailedToRevokeSubkeyException
        of FailedToRevokePrimaryKey:
            raise new FailedToRevokePrimaryKeyException
        of FailedToEncryptKey:
            raise new FailedToEncryptKeyException
        of CertMaybeRevoked:
            if raiseMaybeRevoked:
                raise new CertMaybeRevokedException
        of FoundEncryptedSecretButNoPasswordHandler:
            raise new FoundEncryptedSecretButNoPasswordHandlerException
        of PasswordSetToAtomicButMissingEncryptedKeyid:
            raise new PasswordSetToAtomicButMissingEncryptedKeyidException
        of IncorrectPasswordForSecretKey:
            raise new IncorrectPasswordForSecretKeyException
        of IncorrectKeyFlags:
            raise new IncorrectKeyFlagsException
        of CertIsInvalid:
            raise new CertIsInvalidException
        of FailedToConvertCString:
            discard

proc getValidLengthSeconds(validLength : uint32 or Duration) : uint32 =

    when validLength is Duration:
        let length = validLength.inSeconds()
        if length > uint32.high:
            raise new OverflowDefect
        return uint32 length
    else:
        return validLength

proc newSubkey*(keyFlags : set[KeyTypes],  cipher : CryptSuites = Cv25519, validLength : uint32 or Duration = uint32 0) : UninitiatedSubkey = 
    ##  Creates a new UninitiatedSubkey which can be turned into a subkey in [NewCert]
    ##  
    ##  Note: While you can put arbitrary ciphers for a given key, the ciphers are unknowable due to Sequoia side reasons.
    ##  
    ##  Thus, I recommend you not use whacky combinations of ciphers, unless you want reconstruct your ciphers using the flags and validLength, 
    ##  which i do not recommend as this is a a very recognizable fingerprint.
    ## 
    ##  ValidLength is in seconds, see [newCert] for more detail
    let validLengthSeconds = getValidLengthSeconds(validLength) 
    return UninitiatedSubkey(cipher : cipher, keyFlags : keyFlags, expires : validLengthSeconds != 0, validLengthSeconds : validLengthSeconds)


proc packKeyflags(a : set[KeyTypes]) : uint8 =
    return cast[uint8](a)

proc bitfieldToFlags(a : uint8) : set[KeyTypes] =
    return cast[set[KeyTypes]](a)



proc setupKeys(a : var Cert, keyCount : int, keyids : openArray[string], expireTimes : openArray[uint64], 
                creationTimes : openArray[uint64] , keyFlags : openArray[uint8]) =
    var output : Table[string, Subkey]
    let keyFlags = keyFlags.map(x=>bitfieldToFlags(x))
    a.validLengthSeconds = expireTimes[0]
    a.expires = a.validLengthSeconds != 0
    a.creationTime = creationTimes[0]
    a.primaryKeyId = keyids[0]

    for i in 1 .. keycount:
        let flags = keyFlags[i]
        let creationTime = creationTimes[i]
        let validKeyLength = expireTimes[i]
        let keyid  = keyids[i]


        var subkey = Subkey(
            keyFlags : flags,
            keyId : keyid,
            expires : validKeyLength != 0,
            validLengthSeconds : validKeyLength,
            creationTime : creationTime,
        )

        output[keyid] = subkey

    a.subkeys = output

proc createGenerateKey(a : UninitiatedSubkey) : GenerateKeyStructImp =
    doAssert(a.keyFlags.len() != 0, "Keys must have at least 1 flag, you cannot generate empty keys.")
    if a.validLengthSeconds != 0 and 100 > a.validLengthSeconds:
        raise new MustBeLiveForMoreThan100SecondsException

    return GenerateKeyStructImp(key_flags_bitfield : packKeyflags(a.keyFlags), cipher : a.cipher, expires : a.expires, expire_length_seconds : a.validLengthSeconds)

proc createArrayOfKeys(a : seq[GenerateKeyStructImp]) : ptr UncheckedArray[GenerateKeyStructImp] =
    let newArray = cast[ptr UncheckedArray[GenerateKeyStructImp]](create(GenerateKeyStructImp, len(a)))
    for i, x in enumerate a:
        newArray[i] = x
    return newArray

proc createCStringArray(a : openArray[string]  | seq[string]) : (ptr UncheckedArray[cstring], uint8) =
    let length = a.len()
    var sum = 0
    if length == 0:
        let newArray = cast[ptr UncheckedArray[cstring]](create(cstring, 1))
        return (newArray, 0)
    for i in a:
        sum+=i.len()
    var newArray = cast[ptr UncheckedArray[cstring]](create(cstring, sum))
    
    for i in 0 .. a.high:
        newArray[i] = cstring a[i]
    return (newArray, uint8 length)

proc createCStringArray(a : string) : (ptr UncheckedArray[cstring], uint8) =
    let sum = len(a)
    var newArray = cast[ptr UncheckedArray[cstring]](create(cstring, sum))
    newArray[0] = a
    return (newArray, 1)


proc getLengthOfFixedLengthTypeSeq[T](a : openArray[T] | seq[T]) : int =
    for i in a:
        result += sizeof(i)
    #Not sure I need this yet
    #result -= sizeof(a[0])

proc freeRustStringArray(a : seq[cstring]) = 
    for x in a: free_rust x


proc createPasswordHandlerImpl(a : PasswordHandler ) : PasswordHandlerImpl =
    result.key_count = a.keyCount
    result.global_password = cstring a.globalPassword
    result.use_atomic_password = a.use_atomic_password
    result.initialized = true
    result.keyids = createCStringArray(a.keyids)[0]
    result.password_by_keyid = createCStringArray(a.passwordByKeyid)[0]
proc freePasswordImpl(a : PasswordHandlerImpl) =
    if a.initialized:
        if a.use_atomic_password:
            dealloc a.keyids
            dealloc a.password_by_keyid

proc parseKeys(a : GenerationOutput) : Cert =

    let keyFlags = a.key_flags.toOpenArray(0, int a.key_count-1).toSeq()
    let creationTimes = a.creation_times.toOpenArray(0, int a.key_count-1).toSeq()
    let validLengths = a.subkey_valid_length.toOpenArray(0, int a.key_count-1).toSeq()

    if a.user_id_count > 0:
        let userids = a.user_ids.toOpenArray(0, int a.user_id_count-1).toSeq()
        result.userids = userids.map(x => $x)
        freeRustStringArray userids
        free_rust a.user_ids

    free_rust_array(a.key_flags, uint64 getLengthOfFixedLengthTypeSeq(keyFlags))
    free_rust_array(a.creation_times, uint64 getLengthOfFixedLengthTypeSeq(creationTimes))
    free_rust_array(a.subkey_valid_length, uint64 getLengthOfFixedLengthTypeSeq(validLengths))

    setupKeys(result, int a.key_count-1, ($a.keyids).split(";"), validLengths, creationTimes, keyFlags)

    result.fingerprint = $a.cert_fingerprint
    result.private = PrivateKey($a.private_key)
    result.public = PublicKey($a.public_key)
    
    free_rust a.cert_fingerprint
    free_rust a.public_key
    free_rust a.private_key
    free_rust a.keyids


proc newCert*(subkeySeq : seq[UninitiatedSubkey] | openArray[UninitiatedSubkey], primaryCipher : CryptSuites = Cv25519, user_ids : seq[string] = @[], validLength : uint32 | Duration = uint32 0) : Cert =
    ##  Generates a new PGP Cert.
    ## 
    ##  A given Cert can have a maximum of 256 unique subkeys, this applies to both [scaffoldKey] and this procedure. This can be boosted if people have issues. 
    ##
    ##  validLength takes in either a uint32 in seconds or a duration. 
    ## 
    ##  It is the length of time until it expires, e.g `validLength = 60*60*24*365 = ~1 year` until expiry. If it was made on 1/1/2050 it would expire on 1/1/2051
    ## 
    ##  If Duration > 2^32 (4294967295, uint32.high, ~136.192 years) then a [OverflowDefect] is raised.
    ##  This is because duration on the Rust side uses SystemTime, which has a maximum of u32 when accepting from seconds
    ## 
    ##  The minimum length until it is invalid duration is 100 seconds.
    ## 
    ##  Note: Due to Sequoia's security limitations, you CANNOT make a key which both signs and encrypts data
    ##  This would raise a `KeyCannotBeUsedForTransportAndSigningException <#KeyCannotBeUsedForTransportAndSigningException>`_ inherited from [CertGenerationFailedException]
    ## 
    ##  Also note: Keys Generated with new cert are not encrypted. To encrypt them see `encryptKeys`
    ## 
    ##  There is a maximum of 254 subkeys per key
    ## 
    ## -----------------------------------------------
    ## 
    ## Capable of raising: [CertGenerationFailedException], [FailedToWriteMessageException]

    runnableExamples:
        import nimPGP
        let signingKey = newSubkey({ForSigning})
        let transportKey = newSubkey({ForTransport})
        let cert = newCert(@[signingKey, transportKey])
        let signedMessage = signMessage(cert.private, "Hello, World!")
        let verification = verifySignature(cert.public, signedMessage)
        doAssert verification.isValid == true
        doAssert verification.body == "Hello, World!"

    doAssert(255 > subkeySeq.high, "")

    let validLengthSeconds = getValidLengthSeconds(validLength) 

    if validLengthSeconds != 0 and 100 > validLengthSeconds :
        raise new MustBeLiveForMoreThan100SecondsException

    var createdSubkeys = newSeq[GenerateKeyStructImp](len(subkeySeq))
    for i, subkey in enumerate(subkeySeq):
        createdSubkeys[i] = createGenerateKey(subkey)

    let forInput = createArrayOfKeys(createdSubkeys)
    let user_ids = createCStringArray(user_ids)

    let rustResult = create_new_pgp_impl(forInput, uint8 len(subkeySeq), uint8(primaryCipher), user_ids[0], user_ids[1], validLengthSeconds != 0, validLengthSeconds)
    
    dealloc forInput 
    dealloc user_ids[0]

    handleExceptions(rustResult.error_code)

    result = parseKeys(rustResult)

proc signMessage*(privateKey : PrivateKey,  message : string, keyid : string or Subkey = "", passwordHandler = PasswordHandler()) : string =
    ##  For the verification procedure, see [verifySignature]
    ## 
    ## -----------------------------------------------
    ## 
    ##  Capable of raising the following exceptions: 
    ## 
    ##  [FailedToParseKeyPrivateException], [ExpectedPrivateKeyGotPublicException], [CertRevokedException], 
    ## 
    ##  [FailedToWriteMessageException], [NoKeyMeetsSelectionException], [IncorrectKeyFlagsException], [CertIsInvalidException]
    ##
    ##  [IncorrectPasswordForSecretKeyException], [FoundEncryptedSecretButNoPasswordHandlerException], [PasswordSetToAtomicButMissingEncryptedKeyidException]
    runnableExamples:
        import nimPGP
        let cert = newCert(@[newSubkey({ForSigning})])
        doAssert (verifySignature(cert.public,signMessage(cert.private, "Hello!"))).isValid == true

    let cstringKeyid  =
        when keyid is Subkey:
            cstring keyid.keyId
        else:   
            if keyid == "":
                nil
            else:
                cstring keyid

    let passwordHandlerImpl = createPasswordHandlerImpl(passwordHandler)            
    let rustResult = sign_message_impl(cstring privateKey, cstring message, cstringKeyid, cstringKeyid != nil, passwordHandlerImpl.initialized, passwordHandlerImpl)
    freePasswordImpl passwordHandlerImpl
    handleExceptions(rustResult.error_code)
    result = $rustResult.message
    free_rust rustResult.message


proc sendMessage*(publicKeys : openArray[PublicKey] or PublicKey or seq[PublicKey], message : string) : string =
    ##  For the inverse procedure, see [DecryptMessage]
    ## 
    ##  Can take in a OpenArray[] or Seq[] if you wish to send to multiple public keys
    ## 
    ## -----------------------------------------------
    ## 
    ##  Capable of raising the following exceptions: 
    ## 
    ##  [FailedToParseKeyPublicException], [CertRevokedException], [NoKeyMeetsSelectionException]
    ## 
    ##  [FailedToWriteMessageException], [CertIsInvalidException]
    runnableExamples:
        import nimPGP
        let cert = newCert(@[newSubkey({ForTransport}), newSubkey({ForSigning})])
        let signedMessage = signMessage(cert.private, "Confidential Information")
        echo sendMessage(cert.public, signedMessage)

    let alloced = createCStringArray(publicKeys)
    let newArray = alloced[0]
    let length = alloced[1]


    let rustResult = send_message_impl(newArray, length, cstring message)
    dealloc newArray

    handleExceptions(rustResult.error_code)

    result = $rustResult.message

    free_rust rustResult.message

    handleExceptions(rustResult.error_code)

proc decryptMessage*(privateKeys : openArray[PrivateKey] or PrivateKey or seq[PrivateKey], message : string, passwordHandler = AtomicPasswordHandler()) : Decryption =
    ##  For the inverse procedure, see [sendMessage]
    ## 
    ##  Can take in multiple private keys, but you must create a password keyring using [newPasswordHandlerKeyring]
    ## 
    ## -----------------------------------------------
    ## 
    ##  Capable of raising the following exceptions: 
    ## 
    ##  [FailedToParseKeyPrivateException], [CertRevokedException], [NoKeyMeetsSelectionException]
    ##
    ##  [ExpectedPrivateKeyGotPublicException], [CertIsInvalidException]
    ##
    ##  [FailedToParseMessageException], [FailedToReadFromBufferException], [PackedParserFailedToRecurseException]
    ## 
    ##  [IncorrectPasswordForSecretKeyException], [FoundEncryptedSecretButNoPasswordHandlerException], [PasswordSetToAtomicButMissingEncryptedKeyidException]
    runnableExamples:
        import nimPGP
        let cert = newCert(@[newSubkey({ForTransport}), newSubkey({ForSigning})])
        let signedMessage = signMessage(cert.private, "Confidential Information")
        let encrypted = sendMessage(cert.public, signedMessage)
        let decrypted = decryptMessage(cert.private, encrypted)
        doAssert decrypted.successful
        echo verifySignature(cert.public, decrypted.body)
    ##  With Keyring
    runnableExamples:
        import nimPGP
        import sequtils
        import sugar
        let pwArray = [newPasswordHandler("two"), newPasswordHandler("one"), newPasswordHandler("three")]
        let subkey = newSubkey({ForTransport})
        let certs = collect(for x in 0 .. 2: encryptKeys(newCert(@[subkey]), pwArray[x]))
        #   They **need** to be atomic handlers in order to know which keys
        #   are used for 
        let atomicPws = collect(for x in 0 .. 2: convertGlobalPasswordToAtomic(certs[x], pwArray[x]))        
        let messages = [certs[0].public.sendMessage("0"), certs[1].public.sendMessage("1"), certs[2].public.sendMessage("2")]
        let macroHandler = newPasswordHandlerKeyring(atomicPws)
        for x in 0 .. messages.high:
            let decrypted = decryptMessage(certs.map(x=>x.private), messages[x], macroHandler)
            doASsert decrypted.successful == true

    let alloced = createCStringArray(privateKeys)
    let newArray = alloced[0]
    let length = alloced[1]

    let passwordHandlerImpl = createPasswordHandlerImpl(passwordHandler)

    let rustResult = decrypt_message_impl(newArray, length, cstring message, use_password = passwordHandlerImpl.initialized, passwordHandlerImpl)
    dealloc newArray
    freePasswordImpl passwordHandlerImpl

    handleExceptions(rustResult.error_code)

    result.body = $rustResult.decrypted_data
    result.successful = rustResult.success

    free_rust rustResult.decrypted_data

proc verifySignature*(publicKeys : openArray[PublicKey] or PublicKey or seq[string],  message: string) : Verification =
    ##  For signing of text, see [signMessage]
    ## 
    ## -----------------------------------------------
    ## 
    ##  Capable of raising the following exceptions: 
    ## 
    ##  [FailedToParseKeyPublicException], [CertRevokedException], [NoKeyMeetsSelectionException]
    ## 
    ##  [FailedToParseMessageException], [FailedToReadFromBufferException], [PackedParserFailedToRecurseException]
    ## 
    ##  [CertIsInvalidException]
    runnableExamples:
        import nimPGP
        let cert = newCert(@[newSubkey({ForSigning})])
        let signedMessage = signMessage(cert.private, "Confidential Information")
        echo verifySignature(cert.public, signedMessage)

    var newArray : ptr UncheckedArray[cstring]
    var length : uint8

    let alloced = createCStringArray(publicKeys)
    newArray = alloced[0]
    length = alloced[1]

     
    let rustResult = verify_signature_impl(newArray, length, cstring message)
    dealloc newArray

    handleExceptions(rustResult.error_code)

    result = Verification(
        isValid : rustResult.is_valid,
        body : $rustResult.literal_body
    )

    free_rust rustResult.literal_body

proc revokeSubkey*(cert : Cert, subkey_keyid : string, reason : ReasonForRevocation = Unspecified, message : string = "", passwordHandler = PasswordHandler()) : Cert =
    ##  To check if a subkey is revoked, see [isSubkeyRevoked]
    ##
    ## -----------------------------------------------
    ## 
    ##  Capable of raising the following exceptions:
    ## 
    ##  [FailedToParseKeyPrivateException], [ExpectedPrivateKeyGotPublicException], [NoKeyMeetsSelectionException]
    ## 
    ##  [FailedToRevokeSubkeyException],  [FailedToWriteMessageException], [CertIsInvalidException]
    ## 
    ##  [IncorrectPasswordForSecretKeyException], [FoundEncryptedSecretButNoPasswordHandlerException], [PasswordSetToAtomicButMissingEncryptedKeyidException]
    runnableExamples:
        import nimPGP
        import tables
        import sequtils
        let cert = newCert(@[newSubkey({ForSigning})])
        # returns a new Cert, does not mutate the previous one
        let subkey = cert.subkeys.keys.toSeq()[0]
        let revokedCert = revokeSubkey(cert, subkey, reason = KeyRetired, "I got bored of this key")
        echo isSubkeyRevoked(revokedCert.public, subkey)
    if cert.private == "":
        raise new CertLacksPrivateKey

    let cstringMessage =
        if message == "":
            nil
        else:
            cstring message

    let passwordHandlerImpl = createPasswordHandlerImpl(passwordHandler)

    let rustResult = revoke_impl(cstring cert.private,  passwordHandlerImpl.initialized, passwordHandlerImpl, revokeSubkey = true, keyid = subkey_keyid, reason = reason, message = cstringMessage)
    freePasswordImpl passwordHandlerImpl

    handleExceptions(rustResult.error_code)

    result = cert
    result.public = $rustResult.public_key
    result.private = $rustResult.private_key

    free_rust rustResult.public_key
    free_rust rustResult.private_key

proc revokePrimaryKey*(cert : Cert, reason : ReasonForRevocation = Unspecified, message : string = "", passwordHandler = PasswordHandler()) : Cert =
    ##  To check if a primary key is revoked, see [isPrimaryKeyRevoked]
    ## 
    ## -----------------------------------------------
    ## 
    ##  Capable of raising the following exceptions:
    ## 
    ##  [FailedToParseKeyPrivateException], [ExpectedPrivateKeyGotPublicException], [NoKeyMeetsSelectionException]
    ## 
    ##  [FailedToRevokePrimaryKeyException],  [FailedToWriteMessageException], [CertIsInvalidException]
    ## 
    ##  [IncorrectPasswordForSecretKeyException], [FoundEncryptedSecretButNoPasswordHandlerException], [PasswordSetToAtomicButMissingEncryptedKeyidException]
    runnableExamples:
        import nimPGP
        let cert = newCert(@[newSubkey({ForSigning})])
        let revokedCert = revokePrimaryKey(cert, reason = KeyRetired, "I got bored of this key")
        echo isPrimaryKeyRevoked(revokedCert.public)

    if cert.private == "":
        raise new CertLacksPrivateKey

    let cstringMessage =
        if message == "":
            nil
        else:
            cstring message
    
    let passwordHandlerImpl = createPasswordHandlerImpl(passwordHandler)
    let rustResult = revoke_impl(cstring cert.private, passwordHandlerImpl.initialized, passwordHandlerImpl, reason = reason, message = cstringMessage)
    freePasswordImpl passwordHandlerImpl
    
    handleExceptions(rustResult.error_code)

    result = cert
    result.public = $rustResult.public_key
    result.private = $rustResult.private_key

    free_rust rustResult.public_key
    free_rust rustResult.private_key

proc isSubkeyRevoked*(key : PublicKey, keyid : string or Subkey) : RevocationStatus =
    ##  To revoke a subkey, see [RevokeSubkey]
    let foundKeyId = 
        when keyid is Subkey:
            cstring keyid.keyId
        else:
            cstring keyid

    let isRevoked = is_revoked_impl(cstring key, true, cstring keyid)
    handleExceptions(isRevoked.error_code)
    return isRevoked.status

proc isPrimaryKeyRevoked*(key : PublicKey) : RevocationStatus =
    ##  To revoke a primary key, see [RevokePrimaryKey]
    let isRevoked = is_revoked_impl(cstring key, false, nil)
    handleExceptions(isRevoked.error_code)
    return isRevoked.status


proc getRecipients*(message : string) : seq[string] =
    ##  Gets the keyids for which a pgp message is encrypted for.
    ##     
    ## -----------------------------------------------
    ## 
    ##  Capable of raising the following exceptions:
    ## 
    ##  [FailedToParseMessageException], [PackedParserFailedToRecurseException], [CertIsInvalidException]
    runnableExamples:
        import nimPGP
        import sequtils
        import sugar
        import tables
        let certOne = newCert(@[newSubkey({ForTransport})])
        let certTwo = newCert(@[newSubkey({ForTransport})])
        let message = sendMessage(certOne.private, "for your eyes only!")
        let recipients = getRecipients message 

        echo recipients.any(x => x in certOne.subkeys)
        echo recipients.any(x => x in certTwo.subkeys)

    let rustResult = get_recipients_impl(cstring message)
   
    handleExceptions(rustResult.error_code)

    result = ($rustResult.message).split(";")

    free_rust rustResult.message


proc encryptKey(a : Cert, keyid : string, newPassword : string, oldPassword : string = "" ) : Cert =
    let rustResult = encrypt_key_impl(cstring a.private, cstring keyid, cstring newPassword, oldPassword != "", cstring oldPassword) 

    handleExceptions(rustResult.error_code)

    result = a
    
    result.public = $rustResult.public_key
    result.private = $rustResult.private_key

    free_rust rustResult.public_key
    free_rust rustResult.private_key

proc encryptKeysFromTable(cert : Cert, keyidToPassword: Table[string, string], oldPasswordTable = initTable[string, string]()) : Cert =
    result = cert
    for keyid, password in keyidToPassword.pairs:
        if keyid in oldPasswordTable:
            let mutation = encryptKey(result, keyid, password, oldPasswordTable[keyid])
            result.private = mutation.private
        else:
            let mutation = encryptKey(result, keyid, password)
            result.private = mutation.private

proc passwordHandlerToTable(a : Cert, b : PasswordHandler) : Table[string, string] =
    if b.initialized:
        if b.useAtomicPassword:
            for (keyid, password) in zip(b.keyids, b.passwordByKeyid):
                result[keyid] = password
        else:
            for keyid in a.primaryKeyId & a.subkeys.keys.toSeq():
                result[keyid] = b.globalPassword

proc encryptKeys*(a : Cert, newPassword : PasswordHandler, oldPassword = PasswordHandler()) : Cert =
    ##  Applies the `newPassword` specifications to the given cert.
    ##  
    ##  Allows for re-encryption of a key with `oldPassword`, which corresponds ot the current decryption method. 
    ##
    ## -----------------------------------------------
    ##
    ##  Capable of raising the following exceptions:
    ## 
    ##  [FailedToParseKeyPrivateException], [ExpectedPrivateKeyGotPublicException], [NoKeyMeetsSelectionException]
    ## 
    ##  [FailedToEncryptKeyException], [FailedToWriteMessageException], [CertIsInvalidException]
    runnableExamples:
        import nimPGP
        let signingKey = newSubkey({ForSigning})
        let transportKey = newSubkey({ForTransport})
        let unencryptedCert = newCert(@[signingKey])
        let pwHandler = newPasswordHandler("password12345!")
        let certEncrypted =encryptKeys(unencryptedCert, pwHandler)

    result = a
    var oldPasswordTable = passwordHandlerToTable(a, oldPassword)
    var newPasswordTable = passwordHandlerToTable(a, newPassword)

    let encrypted = encryptKeysFromTable(a, newPasswordTable, oldPasswordTable)
    result.public = encrypted.public
    result.private = encrypted.private

proc scaffoldKey*(key : PublicKey or PrivateKey) : Cert =
    ##  Takes in a private or public key, and parses the keys.
    ##  If a public key is provided, there will be no private key field.
    ## 
    ## -----------------------------------------------
    ## 
    ##  Capable of raising the following exceptions:
    ## 
    ##  [FailedToParseKeyException], [FailedToWriteMessageException], [CertIsInvalidException]
    runnableExamples:
        import nimPGP
        import tables
        import sequtils
        import json
        let privateKey = parseJson(readFile("./src/tests/foregin_certs.json"))[0]["private"].getStr()
        #-----BEGIN PGP PRIVATE KEY BLOCK-----\n[data]----END PGP PRIVATE KEY BLOCK-----

        echo scaffoldKey(privateKey)
    let rustResult = scaffold_key_impl(cstring key)
    handleExceptions(rustResult.error_code)

    result = parseKeys(rustResult)


proc newPasswordHandler*(globalPassword : string) : GlobalPasswordHandler =
    ##  Creates a password handler where, one password is used for all keys.
    ## 
    ##  There is an overload function which takes in a `Table[string, string]` for atomic password handling.
    ## 
    ##  This also encrypts the primary key.
    ## 
    ##  A GlobalPasswordHandler cannot be used in [newPasswordHandlerKeyring] and needs to be converted into an AtomicPasswordHandler
    ##  using [convertGlobalPasswordToAtomic]. 
    runnableExamples:
        import nimPGP
        let signingKey = newSubkey({ForSigning})
        let transportKey = newSubkey({ForTransport})
        let certUnencrypted = newCert(@[signingKey, transportKey])
        let pwHandler = newPasswordHandler("A really super duper good password!")
        let certEncrypted = encryptKeys(certUnencrypted, pwHandler)
    
    doAssert(globalPassword.high > -1) 
    result.globalPassword = globalPassword
    result.useAtomicPassword = false
    result.initialized = true

proc newPasswordHandler*(keyidToPassword : Table[string, string]) : AtomicPasswordHandler =
    ##  Creates a password handler where, each keyid provided has a unique password.
    ## 
    ##  Not all keyids are required to be in the table, they will not be encrypted.
    runnableExamples:
        import nimPGP
        import sequtils
        import tables
        let signingKey = newSubkey({ForSigning})
        let transportKey = newSubkey({ForTransport})
        let unencryptedCert = newCert(@[signingKey, transportKey])
        let keys = unencryptedCert.primaryKeyId & unencryptedCert.subkeys.keys.toSeq()
        let pwHandler = newPasswordHandler({keys[0] : "a", keys[1] : "b", keys[2] : "c"}.toTable())
        let certEncrypted = encryptKeys(unencryptedCert, pwHandler)

    let keyids = keyidToPassword.keys.toseq()
    let passwords = keyidToPassword.values.toseq()
    #if any passwords are less than 1 character
    doAssert(passwords.map(x => x.high > -1).all(x => x))
    doAssert(result.keyids.deduplicate().high == result.keyids.high, "Duplicate keyids in passwordHandlers are not allowed")

    result.keyids = keyids
    result.passwordByKeyid = passwords
    result.keyCount = uint8 keyids.len()
    result.useAtomicPassword = true
    result.initialized = true

proc convertGlobalPasswordToAtomic*(a : Cert, b : GlobalPasswordHandler) : AtomicPasswordHandler =
    ##  This is usually used in the context of creating a keyring with [newPasswordHandlerKeyring]...
    ##  This design allows you to both have simple to make global keys, and make them work in key rights simply.
    ##  Its needed because otherwise the handler has no way of knowing what keys map to what.
    runnableExamples:
        import sequtils
        import sugar
        import nimPGP
        let signingKey = newSubkey({ForSigning})
        let transportKey = newSubkey({ForTransport})
        let certOne = newCert(@[signingKey, transportKey])
        let certTwo = newCert(@[signingKey, transportKey])

        #Creates a handler that can be now used with [newPasswordHandlerKeyring]
        let handlerOne = convertGlobalPasswordToAtomic(certOne, newPasswordHandler("hellofutureme"))
        let handlerTwo = convertGlobalPasswordToAtomic(certTwo, newPasswordHandler("hellopastme"))

        let encryptedCertOne = encryptKeys(certOne, handlerOne)
        let encryptedCertTwo = encryptKeys(certTwo, handlerTwo)
        
        let encryptedMessage = sendMessage(encryptedCertTwo.public, "hello!")
        let keyring = newPasswordHandlerKeyring(handlerOne, handlerTwo)

        let keys = @[encryptedCertOne, encryptedCertTwo].map(x => x.private)
        
        let unencryptedMessage = decryptMessage(keys, encryptedMessage, keyring)
        doAssert unencryptedMessage.body == "hello!"

    let keyids = a.subkeys.keys.toSeq()
    var passwords : seq[string]
    passwords = collect(for x in 0 .. keyids.high+1: b.globalPassword)
    result.keyids = a.primaryKeyId & keyids
    result.passwordByKeyid = passwords
    result.keyCount = uint8 keyids.len()+1
    result.useAtomicPassword = true
    result.initialized = true


proc newPasswordHandlerKeyring*(handlers : varargs[AtomicPasswordHandler]) : AtomicPasswordHandler =
    result.initialized = true
    result.useAtomicPassword = true
    for handler in handlers:
        result.keyids.add(handler.keyids)
        result.passwordByKeyid.add(handler.passwordByKeyid)
    result.keyCount = uint8 result.keyids.len()
    doAssert(result.keyids.deduplicate().high == result.keyids.high, "Duplicate keyids in passwordHandlers are not allowed")



proc get_version() : string =
    let version = get_version_impl()
    result = $version
    free_rust version


#This is a version check to make sure that you aren't using the wrong version for the given release
when not defined(nimPGPDev) and version != "DEV":
    let libVersion = getVersion().split(".")[0 .. 1]
    let versionSplit = version.split(".")[0 .. 1]
    if libVersion != versionSplit:
        when not defined(installCorrectVersion):
            stdout.styledWriteLine(styleBright, fgRed, "  ---BEGIN ERROR MSG---\n")
            echo "  It seems that your libnimPGP version does NOT Match your nimPGP version.\n"
            echo &"  libnimPGP version found: {libVersion}"
            echo &"  nimPGP version found: {versionSplit}"

            echo "\n"
            echo "  This will cause unpredictable behavior, and potential memory leaks."
            echo "\n  SOLUTION 1:\n"
            
            stdout.styledWriteLine(styleBright, fgYellow, "  To ignore this error, **compile with -d:nimPGPDev**")

            echo "\n  SOLUTION 2:\n"
            echo "  For linux x86 we have precompiled builds."
            echo &"  You can download them and link them manually, from https://publiccdn.albassort.com/nimPGP/releases/{version}/"
            echo "  The install directory is /usr/lib/libnimPGP.so and /usr/include/nimPGP.h"
        
            stdout.styledWriteLine(styleBright, fgYellow, "  Or, you can compile this program with -d:installCorrectVersion (and -d:ssl), this program will download the headers and binary to the working directory")
        
            echo "\n  SOLUTION 3:\n"
            echo "  Lastly, the recommended solution is to compile from source. To do so:"
            stdout.styledWriteLine(styleBright, fgRed, &"  cd ~/.nimble/pkgs2/nimPGP-{version}-*; ./scripts/link.sh")
            echo "  This requires cargo."

            stdout.styledWriteLine(styleBright, fgRed, "  ---END ERROR MSG---")

            quit()
        else:
            let path = &"https://publiccdn.albassort.com/nimPGP/releases/{version}/"
            let client = newHttpClient()
            let binary = client.get(path & "libnimPGP.so")
            let headers = client.get(path & "nimPGP.h")
            doAssert(int(binary.code) == 200 and int(headers.code) == 0, &"Failed to download release! Either the version is incorrect or the ({version}) or the files for that version doesn't exist. If this is expected to work, please open an issue or contact me at CarolineMarceano@albassort.com")
            writeFile("libNIMPGP.so", binary.body)
            writeFile("nimPGP.h", binary.body)
            echo "Downloaded successfully! Please move them to /usr/include for the headers, and, /usr/lib for the library."
            quit()
