{.passl: "-lnimPGP -lgmp -lhogweed -lnettle -ldl -lpthread -lm ".}
{.passl: "-include:nimPGP.h".}
type    
    OutwardCommExceptions* {.size: sizeof(uint8), header: "nimPGP.h", importc: "OutwardCommExceptions".} = enum
        #This needs to be kept in sync with the enum in Rust manually, as the header does not enforce parity...
        NoException,
        FailedToWriteMessage,
        ExpectedPrivateKeyGotPublic,
        FailedToParseKeyPublic,
        FailedToParseKeyPrivate,
        FailedToParseKey,
        CertRevoked,
        CertMaybeRevoked,
        FailedToConvertCString,
        NoKeyMeetsSelection,
        FailedToParseMessage,
        PackedParserFailedToRecurse,
        FailedToReadFromBuffer,
        CertGenerationFailed,
        FailedToRevokeSubkey,
        FailedToRevokePrimaryKey,
        FailedToEncryptKey,
        FoundEncryptedSecretButNoPasswordHandler,
        PasswordSetToAtomicButMissingEncryptedKeyid,
        IncorrectPasswordForSecretKey,
        IncorrectKeyFlags
        CertIsInvalid
    CryptSuites* {.size: sizeof(uint8).} = enum 
        Cv25519
        RSA2k
        RSA3k
        RSA4k
        P256
        P384
        P521
    ReasonForRevocation* {.size: sizeof(uint8).} = enum  
        #Do not modify.
        #https://docs.rs/sequoia-openpgp/latest/src/sequoia_openpgp/types/mod.rs.html#1782-1795
        Unspecified = 0
        KeySuperseded = 1
        KeyCompromised = 2
        KeyRetired = 3
        Unknown = 4
        UIDRetired = 32
        Private = 101
    RevocationStatus* {.size: sizeof(uint8).} = enum  
        Revoked
        Maybe
        NotAsFarAsWeKnow
    KeyTypes* {.size: sizeof(uint8).} = enum
        ForCertOfOtherKeys = 0
        ForSigning = 1
        ForTransport = 2
        ForStorageEncryption = 3 
        ForAuthentication = 5


    GenerationOutput* {.header: "nimPGP.h", importc: "GenerationOutput".} = object
        private_key* : cstring
        public_key* : cstring
        keyids* : cstring
        subkey_valid_length* : ptr UncheckedArray[uint64]
        creation_times* : ptr UncheckedArray[uint64]
        key_flags* : ptr UncheckedArray[uint8]
        error_code* : OutwardCommExceptions
        key_count* : uint8
        user_id_count* : uint8
        user_ids* : ptr UncheckedArray[cstring]
        cert_fingerprint* : cstring
    ValidationOutput* {.header: "nimPGP.h", importc: "ValidationOutput".} = object
        is_valid* : bool
        literal_body* : cstring
        error_code* : OutwardCommExceptions
    DecryptOutput* {.header: "nimPGP.h", importc: "DecryptOutput".} = object
        success* : bool
        decrypted_data* : cstring
        error_code* : OutwardCommExceptions
    GenerateKeyStructImp* {.header: "nimPGP.h", importc: "GenerateKey".} = object
        key_flags_bitfield* : uint8
        cipher* : CryptSuites
        expires* : bool
        expire_length_seconds* : uint64
        error_code* : uint8
    SimpleResponse* {.header: "nimPGP.h", importc: "SimpleResponse".} = object
        error_code* : OutwardCommExceptions
        message* : cstring
    MutatedCert* {.header: "nimPGP.h", importc: "MutatedCert".} = object
        public_key* : cstring
        private_key* : cstring
        error_code* : OutwardCommExceptions
    RevokedStatus* {.header: "nimPGP.h", importc: "RevokeStatus".} = object
        status* : RevocationStatus
        error_code* : OutwardCommExceptions
    PasswordHandlerImpl* {.header: "nimPGP.h", importc: "PasswordHandlerInput".} = object
        global_password* : cstring
        use_atomic_password* : bool
        key_count* : uint8
        keyids* : ptr UncheckedArray[cstring]
        password_by_keyid* : ptr UncheckedArray[cstring]
        initialized* : bool

proc free_rust*(a : pointer)  {.header: "nimPGP.h", importc: "free_rust_pointer", varargs.} 

proc create_new_pgp_impl*(  keys : ptr UncheckedArray[GenerateKeyStructImp], length : uint8, primary_cipher : uint8, 
                            user_ids : ptr UncheckedArray[cstring], user_ids_length : uint8, cert_has_expiration_length : bool = false, 
                            cert_valid_length_seconds : uint32 = 0) : GenerationOutput {.header: "nimPGP.h", importc: "create_new_pgp", varargs.} 

proc sign_message_impl*(private_key : cstring, message : cstring, keyid : cstring = nil, 
                        use_keyid = keyid != nil, use_password : bool, password_handler : PasswordHandlerImpl) : SimpleResponse {.header: "nimPGP.h", importc: "sign_message", varargs.} 

proc decrypt_message_impl*( private_keys : ptr UncheckedArray[cstring], key_count :uint8, message : cstring, 
                            use_password : bool, password_handler : PasswordHandlerImpl) : DecryptOutput {.header: "nimPGP.h", importc: "decrypt_message", varargs.} 

proc revoke_impl*(  private : cstring, has_password : bool, password : PasswordHandlerImpl,  
                    revoke_subkey = false, keyid : cstring = nil, reason : ReasonForRevocation = Unknown, 
                    has_message = false, message : cstring = nil) : MutatedCert  {.header: "nimPGP.h", importc: "revoke_cert", varargs.} 

proc scaffold_key_impl*(key : cstring) : GenerationOutput {.header: "nimPGP.h", importc: "scaffold_key", varargs.} 

proc send_message_impl*(to_public_keys : ptr UncheckedArray[cstring], key_count : uint8, message : cstring) : SimpleResponse {.header: "nimPGP.h", importc: "send_message", varargs.} 
proc verify_signature_impl*(from_public_keys : ptr UncheckedArray[cstring], key_count : uint8, message : cstring) : ValidationOutput {.header: "nimPGP.h", importc: "verify_signature", varargs.} 
proc get_recipients_impl*(message : cstring) : SimpleResponse {.header: "nimPGP.h", importc: "get_recipients", varargs.} 
proc is_revoked_impl*(key : cstring, check_subkey : bool = false, keyid : cstring = nil) : RevokedStatus  {.header: "nimPGP.h", importc: "is_revoked", varargs.} 
proc encrypt_key_impl*(private_key, keyid, new_password : cstring, has_old_password : bool, old_password : cstring) : MutatedCert {.header: "nimPGP.h", importc: "encrypt_key", varargs.} 
proc encrypt_all_keys_impl*(private_key, password : cstring) : MutatedCert {.header: "nimPGP.h", importc: "encrypt_all_keys", varargs.} 
proc free_rust_string_array*(a : ptr UncheckedArray[cstring], length : uint8)  {.header: "nimPGP.h", importc: "free_rust_string_array", varargs.} 

proc free_rust_array*(a : pointer, length_in_bytes : uint64)  {.header: "nimPGP.h", importc: "free_rust_array", varargs.} 

proc get_version_impl*() : cstring  {.header: "nimPGP.h", importc: "get_version", varargs.} 
