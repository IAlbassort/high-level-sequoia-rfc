use anyhow::Error;
use openpgp::cert::prelude::*;
use openpgp::cert::CipherSuite::*;
use openpgp::crypto::Decryptor;
use openpgp::crypto::SessionKey;
use openpgp::packet::key::PublicParts;
use openpgp::packet::key::SubordinateRole;
use openpgp::packet::Key;
use openpgp::packet::Signature;
use openpgp::parse::buffered_reader::BufferedReader;
use openpgp::parse::{PacketParser, PacketParserResult, Parse};
use openpgp::policy::StandardPolicy;
use openpgp::serialize::stream::{Armorer, Encryptor2, LiteralWriter, Message, Signer};
use openpgp::serialize::SerializeInto;
use openpgp::types::Duration;
use openpgp::types::KeyFlags;
use openpgp::types::ReasonForRevocation;
use openpgp::types::RevocationStatus;
use openpgp::types::SymmetricAlgorithm;
use openpgp::Cert;
use openpgp::Packet;
use sequoia_openpgp as openpgp;
use std::collections::HashMap;
use std::ffi::CStr;
use std::ffi::CString;
use std::io;
use std::io::Write;
use std::os::raw::c_char;
use std::ptr;
use std::time::UNIX_EPOCH;
fn ffi_string_from_vec8(input: Vec<u8>) -> *mut c_char {
    return CString::new(input).unwrap().into_raw();
}

fn ffi_string_from_string(input: &str) -> *mut c_char {
    return CString::new(input).unwrap().into_raw();
}

fn ffi_string_to_vec8(input: *const c_char) -> Vec<u8> {
    return unsafe { CStr::from_ptr(input).to_bytes().to_vec() };
}

fn ffi_string_to_string(input: *const c_char) -> String {
    //Should be avoid in favor of ffi_string_to_vec8
    //This is liable to produce exceptions whereas a vec8 isn't.
    return unsafe { CStr::from_ptr(input) }
        .to_str()
        .unwrap()
        .to_string();
}

fn create_rust_string_from_vec8(input: Vec<u8>) -> String {
    return String::from_utf8(input).unwrap();
}

fn create_placeholder_string() -> *mut c_char {
    //We use this because if we just want to create a struct that requires a string
    //We don't want to allocate if in case of the function exiting early
    //With the downside, acting on it will segfault.
    return std::ptr::null_mut::<()>() as *mut c_char;
}

fn get_key_by_keyid(cert: &Cert, keyid: String) -> Option<Key<PublicParts, SubordinateRole>> {
    for key in cert.keys().subkeys().into_iter() {
        if key.key().keyid().to_hex().to_string() == keyid {
            return Some(key.key().clone());
        }
    }
    return None;
}

fn get_key_by_keyid_with_keyflags(
    cert: &Cert,
    keyid: String,
) -> (Option<Key<PublicParts, SubordinateRole>>, Option<KeyFlags>) {
    let p = &StandardPolicy::new();

    for key in cert
        .keys()
        .subkeys()
        .with_policy(p, None)
        .supported()
        .alive()
        .revoked(false)
    {
        if key.key().keyid().to_hex().to_string() == keyid {
            return (Some(key.key().clone()), Some(key.key_flags().unwrap()));
        }
    }
    return (None, None);
}

fn ffi_string_array_to_string_array(a: *const *const c_char, b: u8) -> Vec<String> {
    let mut result = Vec::new();
    unsafe {
        for i in 0..b {
            let test = a.offset(i.into());
            let test2 = ffi_string_to_string(*test);
            result.push(test2)
        }
    }
    return result;
}

fn ffi_string_array_to_2d_vec8(a: *const *const c_char, b: u8) -> Vec<Vec<u8>> {
    let mut result = Vec::new();
    unsafe {
        for i in 0..b {
            let test = a.offset(i.into());
            let test2 = ffi_string_to_vec8(*test);
            result.push(test2)
        }
    }
    return result;
}

fn ffi_string_array_from_string_array(strings: Vec<String>) -> (*const *mut c_char, u8) {
    let size = strings.len() as u8;
    let cstrings: Vec<*mut c_char> = strings
        .into_iter()
        .map(|x| ffi_string_from_string(&x))
        .collect();
    let boxed_value = cstrings.into_boxed_slice();
    let result = Box::into_raw(boxed_value);
    return (result as *const *mut c_char, size);
}

fn u8_to_cipher(a: u8) -> CipherSuite {
    return match a {
        0 => Cv25519,
        1 => RSA2k,
        2 => RSA3k,
        3 => RSA4k,
        4 => P256,
        5 => P384,
        6 => P521,
        _ => panic!("Invalid Cipher!")
    };
}

#[repr(C)]
#[derive(Clone, Copy, PartialEq, PartialOrd, Eq, Ord, Debug)]
enum OutwardCommExceptions {
    NoException,
    FailedToWriteMessage,
    ExpectedPrivateKeyGotPublic,
    FailedToParseKeyPublic,
    FailedToParseKeyPrivate,
    FailedToParseKey,
    CertRevoked,

    //This one is special, it shouldn't return the result if it might be revoked.
    //This is because, if its the only issue, we want the developer to decide if they care.
    CertMaybeRevoked,

    FailedToConvertCString,
    NoKeyMeetsSelection,
    FailedToParseMessage,
    PackedParserFailedToRecurse,
    FailedToReadFromBuffer,
    CertGenerationFailed,
    FailedToRevokeSubkey,
    FailedToRevokePrimaryKey,
    FailedToEncryptKey,

    FoundEncryptedSecretButNoPasswordHandler,
    PasswordSetToAtomicButMissingEncryptedKeyid,
    IncorrectPasswordForSecretKey,
    IncorrectKeyFlags,
    CertIsInvalid,
}

#[repr(C)]
#[derive(Copy, Clone)]
struct GenerateKey {
    key_flags_bitfield: u8,
    user_ids: *const *const c_char,
    user_ids_length: u8,
    cipher: u8,
    expires: bool,
    has_user_ids: bool,
    expire_length_seconds: u32,
}

#[repr(C)]
struct SimpleResponse {
    error_code: OutwardCommExceptions,
    message: *mut c_char,
}

#[repr(C)]
struct GenerationOutput {
    private_key: *mut c_char,
    public_key: *mut c_char,
    //Ideally would be *mut *mut c_char but i value my life and allocing that in rust is a nightmare.
    //They are split by ; on the nim side
    keyids: *mut c_char,
    subkey_valid_length: *mut u64,
    creation_times: *mut u64,
    key_flags: *mut u8,
    error_code: OutwardCommExceptions,
    key_count: u8,
    user_id_count: u8,
    user_ids: *const *mut c_char,
    cert_fingerprint: *mut c_char,
}

#[repr(C)]
struct DecryptOutput {
    error_code: OutwardCommExceptions,
    success: bool,
    decrypted_data: *mut c_char,
}

#[repr(C)]
struct ValidationOutput {
    error_code: OutwardCommExceptions,
    is_valid: bool,
    literal_body: *mut c_char,
}

#[repr(C)]
struct MutatedCert {
    private_key: *mut c_char,
    public_key: *mut c_char,
    error_code: OutwardCommExceptions,
}

#[repr(C)]
struct RevokeStatus {
    status: u8,
    error_code: OutwardCommExceptions,
}

#[repr(C)]
struct PasswordHandlerInput {
    global_password: *const c_char,
    //Atomic password handling is when each subkey has their own password.
    use_atomic_password: bool,
    key_count: u8,
    //Keyids and passwords_by_keyids are a table on the nim side
    //Their arrays are put in her individually, and reassembled by index.
    keyids: *const *const c_char,
    password_by_keyid: *const *const c_char,
    initialized: bool,
}

#[derive(Debug, Clone)]
enum PasswordHandler {
    Atomic {
        keyid_to_password: HashMap<String, Vec<u8>>,
    },
    GlobalPassword {
        global_password: Vec<u8>,
    },
}

#[no_mangle]
extern "C" fn get_version() -> *mut c_char {
    return ffi_string_from_string("1.0.0");
}

fn get_password(handler: PasswordHandler, keyid: String) -> Option<Vec<u8>> {
    return match handler {
        PasswordHandler::GlobalPassword { global_password } => Some(global_password),
        PasswordHandler::Atomic {
            ref keyid_to_password,
        } => {
            if !keyid_to_password.contains_key(&keyid) {
                return None;
            }
            return Some(keyid_to_password[&keyid].clone());
        }
    };
}

fn initialize_password_handler(a: PasswordHandlerInput) -> PasswordHandler {
    //creates a password handler that we can actually use.

    match a.use_atomic_password {
        false => {
            return PasswordHandler::GlobalPassword {
                global_password: ffi_string_to_vec8(a.global_password),
            }
        }
        true => {
            let mut result = HashMap::new();
            let passwords = ffi_string_array_to_2d_vec8(a.password_by_keyid, a.key_count);
            let keyids = ffi_string_array_to_string_array(a.keyids, a.key_count);
            for i in 0..a.key_count {
                result.insert(keyids[i as usize].clone(), passwords[i as usize].clone());
            }
            return PasswordHandler::Atomic {
                keyid_to_password: result,
            };
        }
    }
}

#[no_mangle]
extern "C" fn free_rust_pointer(a: *mut u8) {
    unsafe {
        drop(Box::from_raw(a));
    }
}

#[no_mangle]
extern "C" fn free_rust_array(pointer: *mut u8, size_in_bytes: u64) {
    let slice = ptr::slice_from_raw_parts_mut(pointer, size_in_bytes as usize);
    //   println!("{:?}", slice);
    unsafe {
        let boxy = Box::from_raw(slice);
        drop(boxy);
    }
}

fn get_key_data(cert: &Cert) -> GenerationOutput {
    let placeholder = create_placeholder_string();

    //Result is my solution to the fact that rust has no auto-created returns e.g result in nim
    //But also, to make error handling smooth and atomic. This would be easier if I could pass an Option
    //But thats not FFI friendly.
    let mut result = GenerationOutput {
        error_code: OutwardCommExceptions::NoException,
        public_key: placeholder,
        private_key: placeholder,
        keyids: placeholder,

        key_flags: placeholder as *mut u8,
        subkey_valid_length: placeholder as *mut u64,
        creation_times: placeholder as *mut u64,
        key_count: 0,
        user_id_count: 0,
        user_ids: placeholder as *const *mut c_char,
        cert_fingerprint: placeholder,
    };

    let p = &StandardPolicy::new();
    let mut key_flags: Vec<u8> = Vec::new();
    let mut keyids: Vec<String> = Vec::new();
    let mut expire_times = Vec::new();
    let mut creation_times = Vec::new();

    for key in cert
        .keys()
        .with_policy(p, None)
        .supported()
        .alive()
        .revoked(false)
    {
        let key_expiration = key.key_validity_period();
        let keyid = key.keyid().to_hex().to_string();
        let flags = key.key_flags().unwrap().as_bitfield().as_bytes()[0];

        let creation_time = key.creation_time();
        //TODO: HANDLE BACKWARDS TIME
        let epoch_time = creation_time.duration_since(UNIX_EPOCH).unwrap().as_secs();

        expire_times.push(key_expiration.unwrap_or_default().as_secs());
        key_flags.push(flags);
        keyids.push(keyid);
        creation_times.push(epoch_time);
    }

    let private_armored = cert.as_tsk().armored().to_vec();
    let public_armored = cert.armored().to_vec();

    if private_armored.is_err() || public_armored.is_err() {
        result.error_code = OutwardCommExceptions::FailedToWriteMessage;
        return result;
    }

    let expire_times_box = expire_times.into_boxed_slice();
    let expire_times_ptr = Box::into_raw(expire_times_box);
    let creation_times_box = creation_times.into_boxed_slice();
    let creation_times_ptr = Box::into_raw(creation_times_box);
    let key_flags_box = key_flags.into_boxed_slice();
    let key_flags_ptr = Box::into_raw(key_flags_box);

    result.key_flags = key_flags_ptr as *mut u8;
    result.creation_times = creation_times_ptr as *mut u64;
    result.subkey_valid_length = expire_times_ptr as *mut u64;
    result.public_key = ffi_string_from_vec8(public_armored.unwrap());
    result.private_key = ffi_string_from_vec8(private_armored.unwrap());
    result.keyids = ffi_string_from_string(&keyids.join(";"));
    result.cert_fingerprint = ffi_string_from_string(&cert.fingerprint().to_hex().to_string());

    result.key_count = keyids.len() as u8;

    let p = &StandardPolicy::new();
    let valid_user_ids: Vec<String> = cert
        .with_policy(p, None)
        .unwrap()
        .userids()
        .map(|x| String::from_utf8_lossy(x.value()).to_string())
        .collect();
    if valid_user_ids.len() != 0 {
        let (user_id_pointer, length) = ffi_string_array_from_string_array(valid_user_ids);
        result.user_ids = user_id_pointer;
        result.user_id_count = length;
    }
    return result;
}

#[no_mangle]
extern "C" fn create_new_pgp(
    keys: *const GenerateKey,
    key_length: u8,
    primary_cipher: u8,
    user_ids: *const *const c_char,
    user_ids_length: u8,
    cert_has_expiration_length: bool,
    cert_valid_length_seconds: u32,
) -> GenerationOutput {
    let placeholder = create_placeholder_string();

    //Result is my solution to the fact that rust has no auto-created returns e.g result in nim
    //But also, to make error handling smooth and atomic. This would be easier if I could pass an Option
    //But thats not FFI friendly.
    let mut result = GenerationOutput {
        error_code: OutwardCommExceptions::NoException,
        public_key: placeholder,
        private_key: placeholder,
        keyids: placeholder,

        key_flags: placeholder as *mut u8,
        subkey_valid_length: placeholder as *mut u64,
        creation_times: placeholder as *mut u64,
        key_count: 0,
        user_id_count: 0,
        user_ids: placeholder as *const *mut c_char,
        cert_fingerprint: placeholder,
    };

    let mut cert_builder = CertBuilder::new();
    let primary_key_cipher = u8_to_cipher(primary_cipher);
    cert_builder = cert_builder.set_cipher_suite(primary_key_cipher);

    unsafe {
        for i in 0..key_length {
            let key_ptr = keys.offset(i.into());
            let value: GenerateKey = *key_ptr;
            let new_key_flags = KeyFlags::new(&[value.key_flags_bitfield]);
            let cipher = u8_to_cipher(value.cipher);
            if value.expires {
                let duration = Duration::seconds(value.expire_length_seconds);
                cert_builder = cert_builder.add_subkey(new_key_flags, duration, cipher);
            } else {
                cert_builder = cert_builder.add_subkey(new_key_flags, None, cipher);
            }
        }
    }

    for user_id in ffi_string_array_to_string_array(user_ids, user_ids_length) {
        cert_builder = cert_builder.add_userid(user_id);
    }

    let expiration_length: Option<std::time::Duration> = match cert_has_expiration_length {
        false => None,
        true => Some(Duration::seconds(cert_valid_length_seconds).into()),
    };
    cert_builder = cert_builder.set_validity_period(expiration_length);

    let generate = cert_builder.generate();

    if generate.is_err() {
        // println!("{:?}", generate);
        result.error_code = OutwardCommExceptions::CertGenerationFailed;
        return result;
    };

    let (cert, _) = generate.unwrap();

    return get_key_data(&cert);
}

#[no_mangle]
extern "C" fn sign_message(
    private_key: *const c_char,
    message: *const c_char,
    keyid: *const c_char,
    use_keyid: bool,
    has_password: bool,
    password_handler: PasswordHandlerInput,
) -> SimpleResponse {
    let placeholder = create_placeholder_string();

    let p = &StandardPolicy::new();

    let mut result = SimpleResponse {
        error_code: OutwardCommExceptions::NoException,
        message: placeholder,
    };

    let cert = Cert::from_bytes(&ffi_string_to_vec8(private_key));

    if cert.is_err() {
        result.error_code = OutwardCommExceptions::FailedToParseKeyPrivate;
        return result;
    }

    let cert = cert.unwrap();

    if cert.clone().with_policy(p, None).is_err() {
        result.error_code = OutwardCommExceptions::CertIsInvalid;
        return result;
    };

    if !cert.is_tsk() {
        result.error_code = OutwardCommExceptions::ExpectedPrivateKeyGotPublic;
        return result;
    }

    let is_revoked = cert.revocation_status(p, None);
    match is_revoked {
        RevocationStatus::Revoked(_) => {
            result.error_code = OutwardCommExceptions::CertRevoked;
            return result;
        }
        RevocationStatus::CouldBe(_) => {
            //we don't exit here. If this is an issue will be decided after the work is done by nim.
            result.error_code = OutwardCommExceptions::CertMaybeRevoked;
        }
        RevocationStatus::NotAsFarAsWeKnow => {}
    };

    let mut private_signing_key: Option<Key<PublicParts, SubordinateRole>> = None;
    let mut key_flags: Option<KeyFlags> = None;

    if use_keyid {
        let rust_keyid = ffi_string_to_string(keyid);
        let tuple = get_key_by_keyid_with_keyflags(&cert, rust_keyid);
        private_signing_key = tuple.0;
        key_flags = tuple.1
    } else {
        for key in cert
            .keys()
            .with_policy(p, None)
            .supported()
            .alive()
            .revoked(false)
            .for_signing()
        {
            private_signing_key = Some(key.key().clone().into());
            break;
        }
    }

    if private_signing_key.is_none() {
        result.error_code = OutwardCommExceptions::NoKeyMeetsSelection;
        return result;
    }
    if use_keyid {
        if !key_flags.unwrap().for_signing() {
            result.error_code = OutwardCommExceptions::IncorrectKeyFlags;
            return result;
        }
    }

    let mut buffer = Vec::new();
    let private_key = private_signing_key.unwrap().parts_into_secret().unwrap();
    let private_key_pair;
    if !private_key.has_unencrypted_secret() {
        if !has_password {
            result.error_code = OutwardCommExceptions::FoundEncryptedSecretButNoPasswordHandler;
            return result;
        }

        let handler = initialize_password_handler(password_handler);

        let password = get_password(handler, private_key.keyid().to_hex().to_string());
        if password.is_none() {
            result.error_code = OutwardCommExceptions::PasswordSetToAtomicButMissingEncryptedKeyid;

            return result;
        }
        let password = password.unwrap();

        let decrypt_attempt = private_key.decrypt_secret(&password.into());

        if decrypt_attempt.is_err() {
            result.error_code = OutwardCommExceptions::IncorrectPasswordForSecretKey;
            return result;
        }

        private_key_pair = decrypt_attempt.unwrap().into_keypair().unwrap();
    } else {
        private_key_pair = private_key.into_keypair().unwrap();
    }

    let write_output = || -> Result<(), Box<dyn std::error::Error>> {
        let message_bytes = ffi_string_to_vec8(message);
        let cursor = io::Cursor::new(&mut buffer);
        let pgp_message = Message::new(cursor);
        let pgp_message = Armorer::new(pgp_message).build()?;
        let singing = Signer::new(pgp_message, private_key_pair).build()?;
        let mut writer = LiteralWriter::new(singing).build()?;
        writer.write_all(&message_bytes)?;
        writer.finalize()?;
        Ok(())
    };
    let write_result = write_output();
    if write_result.is_err() {
        // println!("{:?}", write_result);
        result.error_code = OutwardCommExceptions::FailedToWriteMessage;
        return result;
    }

    result.message = ffi_string_from_vec8(buffer);
    return result;
}

#[no_mangle]
extern "C" fn send_message(
    to_public_keys: *const *const c_char,
    key_count: u8,
    message: *const c_char,
) -> SimpleResponse {
    let placeholder = create_placeholder_string();

    let mut result = SimpleResponse {
        error_code: OutwardCommExceptions::NoException,
        message: placeholder,
    };

    let p = &StandardPolicy::new();

    let mut certs = Vec::new();
    for public_key in ffi_string_array_to_2d_vec8(to_public_keys, key_count) {
        let receiving_cert = Cert::from_bytes(&public_key);

        if receiving_cert.is_err() {
            result.error_code = OutwardCommExceptions::FailedToParseKeyPublic;
            return result;
        }

        let receiving_cert = receiving_cert.unwrap();

        if receiving_cert.clone().with_policy(p, None).is_err() {
            result.error_code = OutwardCommExceptions::CertIsInvalid;
            return result;
        };

        let is_revoked = receiving_cert.revocation_status(p, None);
        match is_revoked {
            RevocationStatus::Revoked(_) => {
                result.error_code = OutwardCommExceptions::CertRevoked;
                return result;
            }
            RevocationStatus::CouldBe(_) => {
                //This code is special. It doesn't immediately exit.
                //Because, this may or may not be an issue. The nim side has a switch to raise this or not.
                result.error_code = OutwardCommExceptions::CertMaybeRevoked;
            }
            RevocationStatus::NotAsFarAsWeKnow => {}
        }

        certs.push(receiving_cert.clone());
    }

    let mut recipients = Vec::new();
    for cert in &certs {
        let mut recipients_holder = Vec::new();
        for key in cert
            .keys()
            .with_policy(p, None)
            .supported()
            .alive()
            .revoked(false)
            .for_transport_encryption()
        {
            recipients_holder.push(key)
        }
        if recipients_holder.len() == 0 {
            result.error_code = OutwardCommExceptions::NoKeyMeetsSelection;
            return result;
        }
        //  // println!("len: {:?}", recipients_holder.len());
        recipients.append(&mut recipients_holder);
    }

    //  // println!("{:?}", key_count);
    let rust_message = ffi_string_to_vec8(message);
    let mut buffer = Vec::new();
    let write_output = || -> Result<(), Box<dyn std::error::Error>> {
        let cursor = io::Cursor::new(&mut buffer);
        let pgp_message = Message::new(cursor);
        let pgp_message = Armorer::new(pgp_message).build()?;
        let message_encrypter = Encryptor2::for_recipients(pgp_message, recipients).build()?;
        let mut writer = LiteralWriter::new(message_encrypter).build()?;
        writer.write_all(&rust_message)?;
        writer.finalize()?;
        Ok(())
    };

    let write_output_result = write_output();
    if write_output_result.is_err() {
        // println!("{:?}", write_output_result);
        result.error_code = OutwardCommExceptions::FailedToWriteMessage;
        return result;
    }

    result.message = ffi_string_from_vec8(buffer);
    return result;
}

#[no_mangle]
extern "C" fn verify_signature(
    from_public_keys: *const *const c_char,
    key_count: u8,
    message: *const c_char,
) -> ValidationOutput {
    let placeholder = create_placeholder_string();

    let p = &StandardPolicy::new();

    let mut result = ValidationOutput {
        error_code: OutwardCommExceptions::NoException,
        is_valid: false,
        literal_body: placeholder,
    };

    let mut certs = Vec::new();
    for public_key in ffi_string_array_to_2d_vec8(from_public_keys, key_count) {
        let receiving_cert = Cert::from_bytes(&public_key);

        if receiving_cert.is_err() {
            result.error_code = OutwardCommExceptions::FailedToParseKeyPublic;
            return result;
        }

        //Maybe we should do a revocation check, however, I'm not sure if something being revoked is relvant to
        let receiving_cert = receiving_cert.unwrap();

        if receiving_cert.clone().with_policy(p, None).is_err() {
            result.error_code = OutwardCommExceptions::CertIsInvalid;
            return result;
        };

        let is_revoked = receiving_cert.revocation_status(p, None);
        match is_revoked {
            RevocationStatus::Revoked(_) => {
                result.error_code = OutwardCommExceptions::CertRevoked;
                return result;
            }
            RevocationStatus::CouldBe(_) => {
                //This code is special. It doesn't immediately exit.
                //Because, this may or may not be an issue. The nim side has a switch to raise this or not.
                result.error_code = OutwardCommExceptions::CertMaybeRevoked;
            }
            RevocationStatus::NotAsFarAsWeKnow => {}
        };

        certs.push(receiving_cert.clone());
    }

    let rust_message = ffi_string_to_vec8(message);
    let ppr = PacketParser::from_bytes(&rust_message);
    if ppr.is_err() {
        result.error_code = OutwardCommExceptions::FailedToParseMessage;
        return result;
    }

    let mut ppr = ppr.unwrap();
    let mut is_valid = false;
    let mut body: *mut c_char = placeholder;

    while let PacketParserResult::Some(mut pp) = ppr {
        if let Packet::Signature(sig) = &mut pp.packet {
            for cert in &certs {
                for key in cert.keys() {
                    if sig.verify(key.key()).is_ok() {
                        is_valid = true;
                        break;
                    }
                }

                if is_valid {
                    break;
                }
            }
        }
        //So for some reason, sometimes the message isn't in the packet body.
        //In that case, we need to read the unread segment.
        let mut in_body = true;
        if let Packet::Literal(lit) = &pp.packet {
            if lit.body().len() == 0 {
                in_body = false;
            } else {
                body = ffi_string_from_vec8(lit.body().to_vec())
            }
        }
        if !(in_body) {
            if let Packet::Literal(_) = &pp.packet {
                let packet_buffer = pp.buffer_unread_content();
                if packet_buffer.is_err() {
                    result.error_code = OutwardCommExceptions::FailedToReadFromBuffer;
                    return result;
                }
                body = ffi_string_from_vec8(packet_buffer.unwrap().to_vec());
            }
        }

        // Start parsing the next packet, recursing. Sequoia decided they could only implement this as a 'pseudo iterator'
        let ppr_check = pp.recurse();

        if ppr_check.is_err() {
            result.error_code = OutwardCommExceptions::PackedParserFailedToRecurse;
            return result;
        }
        ppr = ppr_check.unwrap().1;
    }

    result.literal_body = body;
    result.is_valid = is_valid;
    return result;
}

//Theres no benefit to switching over to the helper functions.
//They are less readable and verbose. and require the same steps.
//Using Packet  Parser actually illustrates the processes happening to decrypt a message
//Versus the helper functions which don't even illustrate it in the order in which the events occur.
#[no_mangle]
extern "C" fn decrypt_message(
    private_keys: *const *const c_char,
    key_count: u8,
    message: *const c_char,
    has_password: bool,
    handler: PasswordHandlerInput,
) -> DecryptOutput {
    let placeholder = create_placeholder_string();

    let p = &StandardPolicy::new();

    let mut password_handler: Option<PasswordHandler> = None;
    if has_password {
        password_handler = Some(initialize_password_handler(handler))
    }
    let mut result = DecryptOutput {
        error_code: OutwardCommExceptions::NoException,
        success: false,
        decrypted_data: placeholder,
    };

    let mut certs = Vec::new();
    for public_key in ffi_string_array_to_2d_vec8(private_keys, key_count) {
        let receiving_cert = Cert::from_bytes(&public_key);

        if receiving_cert.is_err() {
            result.error_code = OutwardCommExceptions::FailedToParseKeyPrivate;
            return result;
        }
        let receiving_cert = receiving_cert.unwrap();

        if receiving_cert.clone().with_policy(p, None).is_err() {
            result.error_code = OutwardCommExceptions::CertIsInvalid;
            return result;
        };

        if !receiving_cert.is_tsk() {
            result.error_code = OutwardCommExceptions::ExpectedPrivateKeyGotPublic;
            return result;
        }

        let is_revoked = receiving_cert.revocation_status(p, None);
        match is_revoked {
            RevocationStatus::Revoked(_) => {
                result.error_code = OutwardCommExceptions::CertRevoked;
                return result;
            }
            RevocationStatus::CouldBe(_) => {
                //This code is special. It doesn't immediately exit.
                //Because, this may or may not be an issue. The nim side has a switch to raise this or not.
                result.error_code = OutwardCommExceptions::CertMaybeRevoked;
            }
            RevocationStatus::NotAsFarAsWeKnow => {}
        }

        certs.push(receiving_cert.clone());
    }

    let mut keyid_to_key = HashMap::new();
    for cert in &certs {
        for key in cert
            .keys()
            .with_policy(p, None)
            .supported()
            .for_storage_encryption()
            .for_transport_encryption()
        {
            let key_unwrapped = key.key();
            let secret_parts = key_unwrapped.clone().parts_into_secret().unwrap();
            let keyid = key_unwrapped.keyid();
            let keypair;
            if secret_parts.has_unencrypted_secret() {
                keypair = secret_parts.into_keypair().unwrap();
            } else {
                if !has_password {
                    result.error_code =
                        OutwardCommExceptions::FoundEncryptedSecretButNoPasswordHandler;
                    return result;
                }

                let password = get_password(
                    password_handler.clone().unwrap(),
                    keyid.to_hex().to_string(),
                );

                if password.is_none() {
                    result.error_code =
                        OutwardCommExceptions::PasswordSetToAtomicButMissingEncryptedKeyid;
                    return result;
                }

                let decrypt_attempt = secret_parts.decrypt_secret(&password.unwrap().into());

                if decrypt_attempt.is_err() {
                    result.error_code = OutwardCommExceptions::IncorrectPasswordForSecretKey;
                    return result;
                };

                keypair = decrypt_attempt.unwrap().into_keypair().unwrap();
            }

            keyid_to_key.insert(keyid, (cert.fingerprint(), keypair));
        }
    }
    let rust_message = ffi_string_to_vec8(message);

    let mut ppr = PacketParser::from_bytes(&rust_message).unwrap();
    let mut session_key: Option<SessionKey> = None;
    let mut output_string = String::new();
    let mut success = false;
    while let PacketParserResult::Some(mut pp) = ppr {
        if let Packet::SKESK(_) = pp.packet {
            //TODO: Create a key public key which generates SKESKs and decrypt it
            // println!("we have one!: SKESK");
        }
        if let Packet::AED(_) = pp.packet {
            //TODO: Learn about AED packets
            // println!("we have one!: AED");
        }
        if let Packet::SEIP(_) = pp.packet {
            if session_key.is_some() {
                //  // println!("{:?}", session_key.clone().unwrap().to_vec());
                // // println!("{:?}", session_key.clone().unwrap().to_vec().len());

                //There might be a SymmetricAlgorithm identifier in the packet
                //It might be better to parse that then trying all of them, but its only
                //A 16-32 AES decryption so, maybe not that much to worry about.
                for variant in SymmetricAlgorithm::variants() {
                    let decrypt_attempt = pp.decrypt(variant, &session_key.clone().unwrap());
                    if decrypt_attempt.is_ok() {
                        let output_check = pp.data_eof();

                        if output_check.is_err() {
                            result.error_code = OutwardCommExceptions::FailedToReadFromBuffer;
                            return result;
                        }
                        let output = output_check.unwrap().to_vec();

                        // this is messy but hopefully will get solved in code review.

                        //So, the packet body has a header, followed by 5 bytes of null, then the message.
                        //But the header is not of fixed length, so it would be annoying to parse.
                        //The header grows in accordance with the length of the message, e.g 3 bytes, then 4, then 5.
                        //It would be easier just to split after the last null byte. Messages cannot contain null.
                        for i in 0..output.len() - 1 {
                            if output[i] == 0 {
                                if i == output.len() {
                                    //If there is no message
                                    output_string = String::from("");
                                } else if output[i + 1] != 0 {
                                    output_string =
                                        create_rust_string_from_vec8(output[i + 1..].to_vec());
                                }
                            }
                        }

                        success = true;
                        break;
                    }
                }
            }
        }
        if let Packet::PKESK(ref pkesk) = pp.packet {
            let cipher = pkesk.esk();
            let recipient = pkesk.recipient();
            if keyid_to_key.contains_key(recipient) {
                let attempted_session_key = keyid_to_key[recipient].1.clone().decrypt(cipher, None);
                if attempted_session_key.is_ok() {
                    let unsliced = &attempted_session_key.unwrap().to_vec();
                    //According to 4880 5.1 there are 3 bytes used for data integrity and the protocol used
                    //When actually implemented, they use the first byte and the last 2.
                    session_key = Some(unsliced[1..unsliced.len() - 2].into());
                }
            }
        }

        let ppr_check = pp.recurse();

        if ppr_check.is_err() {
            result.error_code = OutwardCommExceptions::PackedParserFailedToRecurse;
            return result;
        }

        ppr = ppr_check.unwrap().1;
    }

    result.success = success;
    result.decrypted_data = ffi_string_from_string(&output_string);
    return result;
}

#[no_mangle]
extern "C" fn get_recipients(message: *const c_char) -> SimpleResponse {
    let placeholder = create_placeholder_string();

    let mut result = SimpleResponse {
        error_code: OutwardCommExceptions::NoException,
        message: placeholder,
    };

    let rust_message = ffi_string_to_vec8(message);
    let ppr = PacketParser::from_bytes(&rust_message);
    if ppr.is_err() {
        result.error_code = OutwardCommExceptions::FailedToParseMessage;
        return result;
    }

    let mut ppr = ppr.unwrap();
    let mut recipients = Vec::new();

    while let PacketParserResult::Some(pp) = ppr {
        if let Packet::PKESK(ref pkesk) = pp.packet {
            recipients.push(pkesk.recipient().to_hex());
        }
        let ppr_check = pp.recurse();

        if ppr_check.is_err() {
            result.error_code = OutwardCommExceptions::PackedParserFailedToRecurse;
            return result;
        }
        ppr = ppr_check.unwrap().1;
    }

    println!("{:?}", recipients);
    result.message = ffi_string_from_string(&recipients.join(";"));
    return result;
}

#[no_mangle]
extern "C" fn revoke_cert(
    private_key: *const c_char,
    has_password: bool,
    password_handler: PasswordHandlerInput,
    revoke_subkey: bool,
    keyid: *const c_char,
    reason: u8,
    has_message: bool,
    message: *const c_char,
) -> MutatedCert {
    let placeholder = create_placeholder_string();

    let mut result = MutatedCert {
        public_key: placeholder,
        private_key: placeholder,
        error_code: OutwardCommExceptions::NoException,
    };

    let p = &StandardPolicy::new();

    let cert = Cert::from_bytes(&ffi_string_to_vec8(private_key));

    if cert.is_err() {
        result.error_code = OutwardCommExceptions::FailedToParseKeyPrivate;
        return result;
    }

    let mut cert = cert.unwrap();

    if !cert.is_tsk() {
        result.error_code = OutwardCommExceptions::ExpectedPrivateKeyGotPublic;
        return result;
    }

    if cert.clone().with_policy(p, None).is_err() {
        result.error_code = OutwardCommExceptions::CertIsInvalid;
        return result;
    };

    let primary_key = cert
        .primary_key()
        .key()
        .clone()
        .parts_into_secret()
        .unwrap();

    let mut primary_keypair;
    if !primary_key.has_unencrypted_secret() {
        if !has_password {
            result.error_code = OutwardCommExceptions::FoundEncryptedSecretButNoPasswordHandler;
            return result;
        }
        let handler = initialize_password_handler(password_handler);
        let password = get_password(handler, primary_key.keyid().to_hex().to_string());
        if password.is_none() {
            result.error_code = OutwardCommExceptions::PasswordSetToAtomicButMissingEncryptedKeyid;
            return result;
        }

        let decrypt_attempt = primary_key
            .clone()
            .decrypt_secret(&password.unwrap().into());

        if decrypt_attempt.is_err() {
            result.error_code = OutwardCommExceptions::IncorrectPasswordForSecretKey;
            return result;
        };

        primary_keypair = decrypt_attempt.unwrap().into_keypair().unwrap();
    } else {
        primary_keypair = primary_key.into_keypair().unwrap();
    }

    if revoke_subkey {
        let rust_keyid = ffi_string_to_string(keyid);
        let potential_keyid = get_key_by_keyid(&cert, rust_keyid);
        if potential_keyid.is_none() {
            result.error_code = OutwardCommExceptions::NoKeyMeetsSelection;
            return result;
        }
        let key = potential_keyid.unwrap();

        let sig: Result<Signature, Error>;
        if has_message {
            let revoke_reason = ReasonForRevocation::from(reason);
            let rust_message = ffi_string_to_vec8(message);
            sig = SubkeyRevocationBuilder::new()
                .set_reason_for_revocation(revoke_reason, &rust_message)
                .unwrap()
                .build(&mut primary_keypair, &cert, &key, None);
        } else {
            sig = SubkeyRevocationBuilder::new().build(&mut primary_keypair, &cert, &key, None);
        }

        if sig.is_err() {
            result.error_code = OutwardCommExceptions::FailedToRevokeSubkey;
            return result;
        }

        cert = cert.insert_packets(sig.unwrap()).unwrap();
    } else {
        let sig: Result<Signature, Error>;

        if has_message {
            let revoke_reason = ReasonForRevocation::from(reason);
            let revoke_message = ffi_string_to_vec8(message);
            sig = cert.revoke(&mut primary_keypair, revoke_reason, &revoke_message);
        } else {
            sig = cert.revoke(&mut primary_keypair, ReasonForRevocation::Unspecified, b"");
        }

        if sig.is_err() {
            result.error_code = OutwardCommExceptions::FailedToRevokePrimaryKey;
            return result;
        }

        cert = cert.insert_packets(sig.unwrap()).unwrap();
    }

    let private_armored = cert.as_tsk().armored().to_vec();
    let public_armored = cert.armored().to_vec();

    if private_armored.is_err() || public_armored.is_err() {
        result.error_code = OutwardCommExceptions::FailedToWriteMessage;
        return result;
    }

    result.public_key = ffi_string_from_vec8(public_armored.unwrap());
    result.private_key = ffi_string_from_vec8(private_armored.unwrap());

    return result;
}

#[no_mangle]
extern "C" fn is_revoked(
    key: *const c_char,
    check_subkey: bool,
    keyid: *const c_char,
) -> RevokeStatus {
    let mut result = RevokeStatus {
        status: 0,
        error_code: OutwardCommExceptions::NoException,
    };

    let p = &StandardPolicy::new();

    let cert = Cert::from_bytes(&ffi_string_to_vec8(key));
    if cert.is_err() {
        result.error_code = OutwardCommExceptions::FailedToParseKey;
        return result;
    }

    let cert = cert.unwrap();

    if cert.clone().with_policy(p, None).is_err() {
        result.error_code = OutwardCommExceptions::CertIsInvalid;
        return result;
    };

    let mut is_revoked: Option<RevocationStatus> = None;
    let saved_key;
    if check_subkey {
        let rust_keyid = ffi_string_to_string(keyid);
        for key in cert.keys().subkeys().into_iter() {
            if key.keyid().to_hex().to_string() == rust_keyid {
                saved_key = key;
                is_revoked = Some(saved_key.revocation_status(p, None));
                break;
            }
        }
        if is_revoked.is_none() {
            result.error_code = OutwardCommExceptions::NoKeyMeetsSelection;
            return result;
        }
    } else {
        is_revoked = Some(cert.revocation_status(p, None));
    }

    let is_revoked = is_revoked.unwrap();

    match is_revoked {
        RevocationStatus::Revoked(_) => result.status = 0,
        RevocationStatus::CouldBe(_) => result.status = 1,
        RevocationStatus::NotAsFarAsWeKnow => result.status = 2,
    }
    return result;
}

#[no_mangle]
extern "C" fn encrypt_key(
    private_key: *const c_char,
    keyid: *const c_char,
    new_password: *const c_char,
    has_old_password: bool,
    old_password: *const c_char,
) -> MutatedCert {
    let placeholder = create_placeholder_string();

    let p = &StandardPolicy::new();

    let mut result = MutatedCert {
        public_key: placeholder,
        private_key: placeholder,
        error_code: OutwardCommExceptions::NoException,
    };

    let cert = Cert::from_bytes(&ffi_string_to_vec8(private_key));

    if cert.is_err() {
        result.error_code = OutwardCommExceptions::FailedToParseKeyPrivate;
        return result;
    }

    let cert = cert.unwrap();

    if cert.clone().with_policy(p, None).is_err() {
        result.error_code = OutwardCommExceptions::CertIsInvalid;
        return result;
    };

    if !cert.is_tsk() {
        result.error_code = OutwardCommExceptions::ExpectedPrivateKeyGotPublic;
        return result;
    }

    let rust_keyid = ffi_string_to_string(keyid);
    let mut keypair: Option<_> = None;
    let mut is_primary = false;
    for key in cert.keys().secret() {
        if key.keyid().to_hex().to_string() == rust_keyid {
            if !key.has_unencrypted_secret() {
                if !has_old_password {
                    // println!("here?");
                    result.error_code =
                        OutwardCommExceptions::FoundEncryptedSecretButNoPasswordHandler;
                    return result;
                }
                let old_password_bytes = ffi_string_to_vec8(old_password);
                let attempted_decrypt = key
                    .key()
                    .clone()
                    .parts_into_secret()
                    .unwrap()
                    .decrypt_secret(&old_password_bytes.into());

                if attempted_decrypt.is_err() {
                    // println!("{:?}", attempted_decrypt);
                    result.error_code = OutwardCommExceptions::IncorrectPasswordForSecretKey;
                    return result;
                }
                keypair = Some(attempted_decrypt.unwrap().parts_into_secret().unwrap());
                is_primary = key.primary();
            } else {
                keypair = Some(key.key().clone());
                is_primary = key.primary();
            }
            break;
        }
    }

    if keypair.is_none() {
        result.error_code = OutwardCommExceptions::NoKeyMeetsSelection;
        return result;
    }

    let password = ffi_string_to_vec8(new_password);
    let packet_untyped = keypair
        .unwrap()
        .clone()
        .encrypt_secret(&password.clone().into());

    if packet_untyped.is_err() {
        result.error_code = OutwardCommExceptions::FailedToEncryptKey;
        return result;
    }

    let typed_packed: Packet = match is_primary {
        true => packet_untyped.unwrap().role_into_primary().into(),
        false => packet_untyped.unwrap().role_into_subordinate().into(),
    };

    let cert = cert.insert_packets(typed_packed).unwrap();

    let private_armored = cert.as_tsk().armored().to_vec();
    let public_armored = cert.armored().to_vec();

    if private_armored.is_err() || public_armored.is_err() {
        result.error_code = OutwardCommExceptions::FailedToWriteMessage;
        return result;
    }

    result.public_key = ffi_string_from_vec8(public_armored.unwrap());
    result.private_key = ffi_string_from_vec8(private_armored.unwrap());

    return result;
}

#[no_mangle]
extern "C" fn scaffold_key(key: *const c_char) -> GenerationOutput {
    let p = &StandardPolicy::new();

    let placeholder = create_placeholder_string();

    let mut result = GenerationOutput {
        error_code: OutwardCommExceptions::NoException,
        public_key: placeholder,
        private_key: placeholder,
        keyids: placeholder,
        key_flags: placeholder as *mut u8,
        subkey_valid_length: placeholder as *mut u64,
        creation_times: placeholder as *mut u64,
        key_count: 0,
        user_id_count: 0,
        user_ids: placeholder as *const *mut c_char,
        cert_fingerprint: placeholder,
    };

    let cert = Cert::from_bytes(&ffi_string_to_vec8(key));

    if cert.is_err() {
        result.error_code = OutwardCommExceptions::FailedToParseKey;
        return result;
    }
    let cert = cert.unwrap();

    if cert.clone().with_policy(p, None).is_err() {
        result.error_code = OutwardCommExceptions::CertIsInvalid;
        return result;
    };

    return get_key_data(&cert);
}
